package com.xcorps.bookingservice.repository;

import com.xcorps.bookingservice.models.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends MongoRepository<Booking, String> {

    Boolean existsBySeats(List<String> seats);

    Boolean existsByUserName(String userName);

    List<Booking> findAllByUserName(String UserName);

}
