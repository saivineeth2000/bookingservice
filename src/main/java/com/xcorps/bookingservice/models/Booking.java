package com.xcorps.bookingservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "bookings")
public class Booking {

    @Id
    private String id;

    private String bookingId;

    private String theaterName;

    private String cityName;

    private String movieName;

    private int cost;

    private String userName;

    private PaymentStatus status;

    private List<String> seats;

    public Booking(String cityName, String theaterName, String movieName, int cost, List<String> seats, String userName) {
        this.cityName = cityName;
        this.theaterName = theaterName;
        this.movieName = movieName;
        this.cost = cost;
        this.seats = seats;
        this.userName = userName;
        status = PaymentStatus.pending;
    }

}
