package com.xcorps.bookingservice.models;

public enum PaymentStatus {

    pending,

    success
}
