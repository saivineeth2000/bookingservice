package com.xcorps.bookingservice.models;

public enum PaymentMethod {

    CreditCard,

    DebitCard,

    UPI,

    NetBanking
}
