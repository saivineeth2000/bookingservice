package com.xcorps.bookingservice.service;

import com.xcorps.bookingservice.models.Booking;
import com.xcorps.bookingservice.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Service
@Slf4j
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;


    public Boolean createBooking(Booking booking){
        if(bookingRepository.existsBySeats(booking.getSeats())){
            log.warn("Seats already occupied, Select different set of seats");
            return false;
        }else{
           booking.setBookingId(UUID.randomUUID().toString());
           bookingRepository.save(booking);
           log.info("Booking Created {}", booking);
           return true;
        }
    }

    public ResponseEntity<?> bookingHistoryByUserName(String userName){
        if(bookingRepository.existsByUserName(userName)) {
                return ResponseEntity.ok(bookingRepository.findAllByUserName(userName));
        }else{
            log.warn("User doesn't exist");
            return ResponseEntity.ok("User Doesn't Exist");
        }
    }
}
