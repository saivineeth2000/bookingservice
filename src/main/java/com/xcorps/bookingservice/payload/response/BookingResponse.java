package com.xcorps.bookingservice.payload.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BookingResponse {

    private String bookingId;

    //private String userId;

    private String cityName;

    private String theaterName;

    private String movieName;

    private int cost;

    private List<String> seats;

    private String userName;

    public BookingResponse(String bookingId, String cityName, String theaterName, String movieName, int cost, List<String> seats, String userName){
        this.bookingId = bookingId;
        this.cityName = cityName;
        this.theaterName = theaterName;
        this.movieName = movieName;
        this.cost = cost;
        this.seats = seats;
        this.userName = userName;
    }

    public BookingResponse(String bookingId, String cityName, String theaterName, String movieName, int cost, List<String> seats){
        this.bookingId = bookingId;
        this.cityName = cityName;
        this.theaterName = theaterName;
        this.movieName = movieName;
        this.cost = cost;
        this.seats = seats;
    }


    //private String showId;

    //private String paymentId;

    //private Status status;

    //private List<Long> seatIds;
}
