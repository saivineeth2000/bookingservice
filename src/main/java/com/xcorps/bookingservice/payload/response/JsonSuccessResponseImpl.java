package com.xcorps.bookingservice.payload.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JsonSuccessResponseImpl {

    private String status;

    private String message;
}
