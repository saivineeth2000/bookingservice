package com.xcorps.bookingservice.payload.request;

import com.xcorps.bookingservice.models.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest {

    private String bookingId;

    private int cost;

    private PaymentMethod paymentMethod;
}
