package com.xcorps.bookingservice.payload.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import java.util.List;

@Data
@Builder
public class BookingRequest {


    @NotBlank(message = "Theater name should not be blank")
    private String theaterName;

    @NotBlank(message = "City name should not be blank")
    private String cityName;

    @NotBlank(message = "Movie name should not be blank")
    private String movieName;

    @NotBlank(message = "User name should not be blank")
    private String userName;

    // private String screenNumber;

    @NotBlank(message = "List of seats should not be blank")
    private List<String> seats;

    @NotBlank(message = "Cost should not be blank")
    @Min(200)
    private int cost;


}
