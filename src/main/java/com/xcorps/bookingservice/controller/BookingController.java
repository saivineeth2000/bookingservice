package com.xcorps.bookingservice.controller;


import com.xcorps.bookingservice.models.Booking;
import com.xcorps.bookingservice.payload.request.BookingRequest;
import com.xcorps.bookingservice.service.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;

import static com.xcorps.bookingservice.utils.JsonResponse.failureResponse;
import static com.xcorps.bookingservice.utils.JsonResponse.successResponse;

@RestController
@Slf4j
public class BookingController {
    @Autowired
    BookingService bookingService;

    /**
     * Creates a "ticket"
     * @param bookingRequest containing all the booking information.
     * @return ResponseEntity with a success or failure JSON response.
     */
    @PostMapping("/bookTicket")
    public ResponseEntity<?> bookTicket(@Valid @RequestBody BookingRequest bookingRequest){

        Booking booking = new Booking(bookingRequest.getCityName(), bookingRequest.getTheaterName(), bookingRequest.getMovieName()
                    , bookingRequest.getCost(), bookingRequest.getSeats(), bookingRequest.getUserName());
        if(bookingService.createBooking(booking)){
            String message = "Booking Created with Seats" + booking.getSeats();
            return new ResponseEntity<>(successResponse(message), HttpStatus.CREATED);
        }else{
            String message = "Booking Creation failed";
            String reason = "Seats already booked";
            return new ResponseEntity<>(failureResponse(message,reason), HttpStatus.CONFLICT);
        }
    }

    /**
     * Returns users' booking history.
     * @param userName containing the users' identity.
     * @return if a booking is present under that user then returns that booking.
     */
    @GetMapping("/getBookingHistoryByUserName")
    public ResponseEntity<?> getBookingHistoryByUserName(@RequestParam("userName") String userName){
        return ResponseEntity.ok(bookingService.bookingHistoryByUserName(userName));
    }


}
