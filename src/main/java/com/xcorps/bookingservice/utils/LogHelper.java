//package com.xcorps.bookingservice.utils;
//
//import java.util.Arrays;
//
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.context.annotation.Configuration;
//
//@Aspect
//@Slf4j
//@Configuration
//public class LogHelper {
//
//    @Before("execution(* com.xcorps.bookingservice.*..*(..))")
//    public void logBefore(JoinPoint joinPoint) {
//        long startTime = System.currentTimeMillis();
//        String methodName = joinPoint.getSignature().getName();
//        String className = joinPoint.getTarget().getClass().getSimpleName();
//        Object[] args = joinPoint.getArgs();
//
//        log.info("Before " + className + "." + methodName + "()");
//    }
//
//    @After("execution(* com.xcorps.bookingservice.*..*(..))")
//    public void logAfter(JoinPoint joinPoint) {
//        String methodName = joinPoint.getSignature().getName();
//        String className = joinPoint.getTarget().getClass().getSimpleName();
//        Object[] args = joinPoint.getArgs();
//
//        log.info("After "  + className + "." + methodName + "()");
//    }
//
//    @Around("execution(* com.xcorps.bookingservice.*..*(..))")
//    public void measureTime(ProceedingJoinPoint joinPoint) throws Throwable {
//        long startTime = System.currentTimeMillis();
//        Object result = joinPoint.proceed();
//        long endTime = System.currentTimeMillis();
//        long timeTaken = endTime - startTime;
//
//        String methodName = joinPoint.getSignature().getName();
//        String className = joinPoint.getTarget().getClass().getSimpleName();
//
//        log.info(className + "." + methodName + "() took " + timeTaken + "ms to execute");
//
//    }
//}
package com.xcorps.bookingservice.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Component
public class LogHelper {

    Logger log = LoggerFactory.getLogger(LogHelper.class);

    @Pointcut(value="execution(* com.xcorps.bookingservice.*.*.*(..) )")
    public void myPointcut() {

    }

    @Around("myPointcut()")
    public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        String methodName = pjp.getSignature().getName();
        String className = pjp.getTarget().getClass().toString();
        Object[] array = pjp.getArgs();
        log.info("method invoked " + className + " : " + methodName + "()" + "arguments : "
                + mapper.writeValueAsString(array));
        Object object = pjp.proceed();
        log.info(className + " : " + methodName + "()" + "Response : "
                + mapper.writeValueAsString(object));
        return object;
    }

}
