package com.xcorps.bookingservice.utils;

import com.xcorps.bookingservice.payload.response.JsonErrorResponseImpl;
import com.xcorps.bookingservice.payload.response.JsonSuccessResponseImpl;

public class JsonResponse {


    /**
     * Generates a success response object with the provided message.
     *
     * @param message The success message to include in the response
     * @return The generated success response object
     */
    public static JsonSuccessResponseImpl successResponse(String message) {
        return JsonSuccessResponseImpl.builder()
                .status("success")
                .message(message)
                .build();
    }

    /**
     * Generates an error response object with the provided message and reason.
     *
     * @param message The error message to include in the response
     * @param reason  The reason or explanation for the error
     * @return The generated error response object
     */
    public static JsonErrorResponseImpl failureResponse(String message, String reason) {
        return JsonErrorResponseImpl.builder()
                .status("failure")
                .message(message)
                .reason(reason)
                .build();
    }

}
